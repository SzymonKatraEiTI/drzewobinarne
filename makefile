CC=g++
LD=g++
CPPFLAGS=-c -Wall -std=c++11
DEBUGFLAGS=-g -DDEBUG

PROJECTNAME=drzewo
OBJDIR=obj
BINDIR=bin
DEBUGDIR=Debug
RELEASEDIR=Release
OBJ=main.o Tests.o BinaryTree.o
DEPS=Tests.hpp BinaryTree.hpp

DEBUGOBJ=$(patsubst %,$(OBJDIR)/$(DEBUGDIR)/%,$(OBJ))
RELEASEOBJ=$(patsubst %,$(OBJDIR)/$(RELEASEDIR)/%,$(OBJ))

all: release


release: $(RELEASEOBJ) | $(BINDIR)/$(RELEASEDIR)
	$(LD) $^ -o $(BINDIR)/$(RELEASEDIR)/$(PROJECTNAME)

$(OBJDIR)/$(RELEASEDIR)/%.o: %.cpp $(DEPS) | $(OBJDIR)/$(RELEASEDIR)
	$(CC) $(CPPFLAGS) $< -o $@

$(OBJDIR)/$(RELEASEDIR):
	mkdir -p $@

$(BINDIR)/$(RELEASEDIR):
	mkdir -p $@


debug: $(DEBUGOBJ) | $(BINDIR)/$(DEBUGDIR)
	$(LD) $^ -o $(BINDIR)/$(DEBUGDIR)/$(PROJECTNAME)

$(OBJDIR)/$(DEBUGDIR)/%.o: %.cpp $(DEPS) | $(OBJDIR)/$(DEBUGDIR)
	$(CC) $(CPPFLAGS) $(DEBUGFLAGS) $< -o $@


$(OBJDIR)/$(DEBUGDIR):
	mkdir -p $@

$(BINDIR)/$(DEBUGDIR):
	mkdir -p $@


clean:
	rm -r -f $(OBJDIR)
	rm -r -f $(BINDIR)

