#include <iostream>

#include "Tests.hpp"

using namespace std;

int main()
{
    Tests::runAll();
    return 0;
}
