#ifndef TESTS_HPP_
#define TESTS_HPP_

#include <string>
#include "BinaryTree.hpp"

using namespace std;

class Tests
{
    private:
    Tests() { }
    static bool assert(bool expression, const string& name, bool writeOnTrue = true);

    public:
    static void runAll();

    static void addElements(BinaryTree& tree, int amount, int margin);

    static void add();
    static void contains();
    static void get();
    static void getSize();
    static void remove1();
    static void remove2();
    static void removeAndGet1();
    static void removeAndGet2();
    static void removeAndGet3();
    static void compare1();
    static void compare2();
    static void clear();
    static void listNodes();
    static void addTrees();
    static void subtractTrees();
    static void copy();
    static void assignmentOperator();
    static void equalsOperator();
    static void nonEqualsOperator();
    static void addOperatorTrees1();
    static void addOperatorTrees2();
    static void subtractOperatorTrees1();
    static void subtractOperatorTrees2();
    static void addOperatorValue1();
    static void addOperatorValue2();
    static void removeOperatorValue1();
    static void removeOperatorValue2();
    static void removeOperatorNode();
    static void accessorOperator();
};

#endif
