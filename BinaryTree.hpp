#ifndef BINARYTREE_HPP_
#define BINARYTREE_HPP_

#include <vector>

using namespace std;

class BinaryTree
{
    public:
    class Node;

    private:
    Node* m_root;

    static void freeRecursive(Node* node);
    static void computeSizeRecursive(const Node* node, int* size);
    static Node* findMin(Node* node);
    static bool compareRecursive(const Node* a, const Node* b);
    static void listNodesRecursive(const Node* node, vector<const Node*>& result);

    public:
    BinaryTree();
    BinaryTree(const BinaryTree& other);
    ~BinaryTree();

    const Node* add(int value);

    int remove(int value); // returns number of removed values
    bool remove(const Node* node);

    const Node* get(int value) const;
    const Node* getRoot() const;

    int getSize() const;

    bool contains(int value) const;

    bool compareWith(const BinaryTree& other) const;

    void listNodes(vector<const Node*>& result) const;

    void clear();

    void addTree(const BinaryTree& summand);
    void subtractTree(const BinaryTree& subtrahend);

    static void addTrees(const BinaryTree& a, const BinaryTree& b, BinaryTree& result);
    static void subtractTrees(const BinaryTree& minuend, const BinaryTree& subtrahend, BinaryTree& result);

    BinaryTree& operator=(const BinaryTree& other);
    bool operator==(const BinaryTree& other) const;
    bool operator!=(const BinaryTree& other) const;
    BinaryTree operator+(const BinaryTree& value) const;
    BinaryTree& operator+=(const BinaryTree& value);
    BinaryTree operator-(const BinaryTree& value) const;
    BinaryTree& operator-=(const BinaryTree& value);
    BinaryTree operator+(const int& value) const;
    BinaryTree& operator+=(const int& value);
    BinaryTree operator-(const int& value) const;
    BinaryTree& operator-=(const int& value);
    BinaryTree& operator-=(const Node*& node);
    const Node* operator[](const int& value) const;
};

class BinaryTree::Node
{
    public:
    Node* parent;
    Node* left;
    Node* right;
    int value;

    Node();
    Node(int value);
    Node(int value, Node* parent);
    Node(int value, Node* parent, Node* left, Node* right);
};

#endif
