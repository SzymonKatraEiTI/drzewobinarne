#include "BinaryTree.hpp"

BinaryTree::BinaryTree()
{
    m_root = nullptr;
}
BinaryTree::BinaryTree(const BinaryTree& other) : BinaryTree::BinaryTree()
{
    vector<const BinaryTree::Node*> nodes;
    other.listNodes(nodes);

    for (vector<const BinaryTree::Node*>::iterator i = nodes.begin(); i != nodes.end(); i++) this->add((*i)->value);
}
BinaryTree::~BinaryTree()
{
    clear();
}

const BinaryTree::Node* BinaryTree::add(int value)
{
    if (m_root == nullptr)
    {
        m_root = new Node(value);
        return m_root;
    }

    Node* parent = m_root;

    while (parent != nullptr)
    {
        if (value < parent->value)
        {
            if (parent->left == nullptr)
            {
                parent->left = new Node(value, parent);
                return parent->left;
            }
            else parent = parent->left;
        }
        else
        {
            if (parent->right == nullptr)
            {
                parent->right = new Node(value, parent);
                return parent->right;
            }
            else parent = parent->right;
        }
    }

    return nullptr;
}

int BinaryTree::remove(int value)
{
    int number = 0;
    const BinaryTree::Node* node;
    while ((node = get(value)) != nullptr)
    {
        if (remove(node)) number++;
    }

    return number;
}
bool BinaryTree::remove(const BinaryTree::Node* node)
{
    if (node == nullptr) return false;

    BinaryTree::Node* current = const_cast<Node*>(node); // because user have const one's but we need to modify it
    BinaryTree::Node* parent = current->parent;

    if (node->left == nullptr && node->right == nullptr) // no childs so we can just disconnect this node
    {
        if (node == m_root)
        {
            m_root = nullptr;
        }
        else
        {
            if (parent->left == node) parent->left = nullptr;
            else parent->right = nullptr;
        }

        delete node;

        return true;
    }

    if (current->left != nullptr && current->right == nullptr) // child on the left so we can replace node with it
    {
        if (node == m_root)
        {
            m_root = current->left;
        }
        else
        {
            if (parent->left == current) parent->left = current->left;
            else parent->right = current->left;

            current->left->parent = parent;
        }

        delete current;

        return true;
    }

    if (current->left == nullptr && current->right != nullptr) // child on the right so we can replace node with it
    {
        if (node == m_root)
        {
            m_root = current->right;
        }
        else
        {
            if (parent->left == current) parent->left = current->right;
            else parent->right = current->right;

            current->right->parent = parent;
        }

        delete current;

        return true;
    }

    if (node->left != nullptr && node->right != nullptr) // two childs, we need to find minimum element in right subtree and replace with it
    {
        Node* minimum = BinaryTree::findMin(node->right);

        current->value = minimum->value;

        remove(minimum);

        return true;
    }

    return false;
}

const BinaryTree::Node* BinaryTree::get(int value) const
{
    if (m_root == nullptr) return nullptr;

    Node* current = m_root;
    while (current->value != value)
    {
        if (value < current->value)
        {
            if (current->left == nullptr) return nullptr;
            current = current->left;
        }
        else
        {
            if (current->right == nullptr) return nullptr;
            current = current->right;
        }
    }

    return current;
}
const BinaryTree::Node* BinaryTree::getRoot() const
{
    return m_root;
}

int BinaryTree::getSize() const
{
    int size = 0;
    BinaryTree::computeSizeRecursive(m_root, &size);
    return size;
}

bool BinaryTree::contains(int value) const
{
    return get(value) != nullptr;
}

bool BinaryTree::compareWith(const BinaryTree& other) const
{
    return BinaryTree::compareRecursive(m_root, other.m_root);
}

void BinaryTree::listNodes(vector<const BinaryTree::Node*>& result) const
{
    BinaryTree::listNodesRecursive(m_root, result);
}

void BinaryTree::clear()
{
    BinaryTree::freeRecursive(m_root);
    m_root = nullptr;
}

void BinaryTree::addTree(const BinaryTree& summand)
{
    vector<const BinaryTree::Node*> nodes;

    summand.listNodes(nodes);

    for (vector<const BinaryTree::Node*>::iterator i = nodes.begin(); i != nodes.end(); i++) this->add((*i)->value);
}
void BinaryTree::subtractTree(const BinaryTree& subtrahend)
{
    vector<const BinaryTree::Node*> nodes;

    subtrahend.listNodes(nodes);

    for (vector<const BinaryTree::Node*>::iterator i = nodes.begin(); i != nodes.end(); i++)
    {
        this->remove((*i)->value);
    }
}

void BinaryTree::freeRecursive(BinaryTree::Node* node)
{
    if (node != nullptr)
    {
        BinaryTree::freeRecursive(node->left);
        BinaryTree::freeRecursive(node->right);
        delete node;
    }
}

void BinaryTree::computeSizeRecursive(const BinaryTree::Node* node, int* size)
{
    if (node != nullptr)
    {
        BinaryTree::computeSizeRecursive(node->left, size);
        BinaryTree::computeSizeRecursive(node->right, size);
        (*size)++;
    }
}

BinaryTree::Node* BinaryTree::findMin(BinaryTree::Node* node)
{
    Node* current = node;

    while (current->left != nullptr) current = current->left;

    return current;
}

bool BinaryTree::compareRecursive(const BinaryTree::Node* a, const BinaryTree::Node* b)
{
    if (a == nullptr && b == nullptr) return true;
    if (a == nullptr && b != nullptr) return false;
    if (a != nullptr && b == nullptr) return false;

    if (a->value != b->value) return false;

    return BinaryTree::compareRecursive(a->left, b->left) && compareRecursive(a->right, b->right);
}

void BinaryTree::listNodesRecursive(const BinaryTree::Node* node, vector<const BinaryTree::Node*>& result)
{
    if (node == nullptr) return;
    result.push_back(node);
    BinaryTree::listNodesRecursive(node->left, result);
    BinaryTree::listNodesRecursive(node->right, result);
}

void BinaryTree::addTrees(const BinaryTree& a, const BinaryTree& b, BinaryTree& result)
{
    result = a;
    result.addTree(b);
}

void BinaryTree::subtractTrees(const BinaryTree& minuend, const BinaryTree& subtrahend, BinaryTree& result)
{
    result = minuend;
    result.subtractTree(subtrahend);
}

BinaryTree& BinaryTree::operator=(const BinaryTree& other)
{
    if (this != &other)
    {
        this->clear();

        vector<const BinaryTree::Node*> nodes;
        other.listNodes(nodes);

        for (vector<const BinaryTree::Node*>::iterator i = nodes.begin(); i != nodes.end(); i++) this->add((*i)->value);
    }

    return *this;
}
bool BinaryTree::operator==(const BinaryTree& other) const
{
    return this->compareWith(other);
}
bool BinaryTree::operator!=(const BinaryTree& other) const
{
    return !this->compareWith(other);
}
BinaryTree BinaryTree::operator+(const BinaryTree& other) const
{
    BinaryTree tree;
    BinaryTree::addTrees(*this, other, tree);
    return tree;
}
BinaryTree& BinaryTree::operator+=(const BinaryTree& other)
{
    this->addTree(other);
    return *this;
}
BinaryTree BinaryTree::operator-(const BinaryTree& other) const
{
    BinaryTree tree;
    BinaryTree::subtractTrees(*this, other, tree);
    return tree;
}
BinaryTree& BinaryTree::operator-=(const BinaryTree& other)
{
    this->subtractTree(other);
    return *this;
}
BinaryTree BinaryTree::operator+(const int& value) const
{
    BinaryTree tree(*this);
    tree.add(value);
    return tree;
}
BinaryTree& BinaryTree::operator+=(const int& value)
{
    this->add(value);
    return *this;
}
BinaryTree BinaryTree::operator-(const int& value) const
{
    BinaryTree tree(*this);
    tree.remove(value);
    return tree;
}
BinaryTree& BinaryTree::operator-=(const int& value)
{
    this->remove(value);
    return *this;
}
BinaryTree& BinaryTree::operator-=(const BinaryTree::Node*& value)
{
    this->remove(value);
    return *this;
}
const BinaryTree::Node* BinaryTree::operator[](const int& value) const
{
    return this->get(value);
}

BinaryTree::Node::Node() : BinaryTree::Node::Node(0)
{
}
BinaryTree::Node::Node(int value) : BinaryTree::Node::Node(value, nullptr)
{
}
BinaryTree::Node::Node(int value, BinaryTree::Node* parent) : BinaryTree::Node::Node(value, parent, nullptr, nullptr)
{
}
BinaryTree::Node::Node(int value, BinaryTree::Node* parent, BinaryTree::Node* left, BinaryTree::Node* right)
{
    this->value = value;
    this->parent = parent;
    this->left = left;
    this->right = right;
}
