#include "Tests.hpp"

#include <iostream>
#include <iomanip>

using namespace std;

bool Tests::assert(bool expression, const string& name, bool writeOnTrue)
{
    if (!writeOnTrue && expression) return expression;

    cout << "Test | " << setw(30) << left << name << " | " << (expression ? "passed" : "FAILED") << endl;

    return expression;
}

void Tests::runAll()
{
    Tests::add();
    Tests::contains();
    Tests::get();
    Tests::getSize();
    Tests::remove1();
    Tests::remove2();
    Tests::removeAndGet1();
    Tests::removeAndGet2();
    Tests::removeAndGet3();
    Tests::compare1();
    Tests::compare2();
    Tests::clear();
    Tests::listNodes();
    Tests::addTrees();
    Tests::subtractTrees();
    Tests::copy();
    Tests::assignmentOperator();
    Tests::equalsOperator();
    Tests::nonEqualsOperator();
    Tests::addOperatorTrees1();
    Tests::addOperatorTrees2();
    Tests::subtractOperatorTrees1();
    Tests::subtractOperatorTrees2();
    Tests::addOperatorValue1();
    Tests::addOperatorValue2();
    Tests::removeOperatorValue1();
    Tests::removeOperatorValue2();
    Tests::removeOperatorNode();
    Tests::accessorOperator();
}

void Tests::addElements(BinaryTree& tree, int amount, int margin)
{
    for (int m = 0; m < margin; m++)
    {
        for (int i = m + 1; i <= amount; i += margin)
        {
            tree.add(i);
        }
    }
}

void Tests::add()
{
    BinaryTree tree;

    tree.add(5);
    tree.add(1);
    tree.add(7);
    tree.add(6);

    const BinaryTree::Node* root = tree.getRoot();

    if (!Tests::assert(root->value == 5, "add (root)", false)) return;
    if (!Tests::assert(root->left != nullptr && root->right != nullptr, "add (childs)", false)) return;
    if (!Tests::assert(root->left->value == 1 && root->right->value == 7, "add (values of childs)", false)) return;
    if (!Tests::assert(root->left->left == nullptr && root->left->right == nullptr, "add (childs on left)", false)) return;
    if (!Tests::assert(root->right->left != nullptr && root->right->right == nullptr, "add (childs on right)", false)) return;
    if (!Tests::assert(root->right->left->value == 6, "add (value of childs on right)", false)) return;

    Tests::assert(true, "add");
}
void Tests::contains()
{
    BinaryTree tree;
    Tests::addElements(tree, 100, 5);
    for (int i = 1; i <= 100; i++)
    {
        if (!Tests::assert(tree.contains(i), "checkContains (" + to_string(i) + ")", false)) return;
    }
    for (int i = 101; i <= 200; i++)
    {
        if (!Tests::assert(!tree.contains(i), "checkContains (not " + to_string(i) + ")", false)) return;
    }

    Tests::assert(true, "contains");
}
void Tests::get()
{
    BinaryTree tree;
    Tests::addElements(tree, 100, 5);

    for (int i = 1; i <= 100; i++)
    {
        if (!Tests::assert(tree.get(i) != nullptr, "checkGet (" + to_string(i) + ")", false)) return;
    }
    for (int i = 101; i <= 200; i++)
    {
        if (!Tests::assert(tree.get(i) == nullptr, "checkGet (not " + to_string(i) + ")", false)) return;
    }

    Tests::assert(true, "get");
}
void Tests::getSize()
{
    BinaryTree tree;
    Tests::addElements(tree, 100, 5);

    Tests::assert(tree.getSize() == 100, "getSize");
}
void Tests::remove1()
{
    BinaryTree tree;
    tree.add(10);
    tree.add(5);
    tree.add(1);
    tree.add(7);
    tree.add(20);
    tree.add(15);
    tree.add(30);
    tree.add(25);
    tree.add(40);
    tree.add(26);

    tree.remove(20);

    Tests::assert(tree.getRoot()->right->value == 25, "remove1");
}
void Tests::remove2()
{
    BinaryTree tree;
    Tests::addElements(tree, 1000, 5);

    int removedItems = 0;
    for (int i = 1; i <= 1000; i+= 2)
    {
        removedItems += tree.remove(i);
    }

    Tests::assert(tree.getSize() == 1000 - removedItems, "remove2");
}
void Tests::removeAndGet1()
{
    BinaryTree tree;
    tree.add(2);
    tree.add(1);
    tree.add(3);
    tree.remove(1);

    if (!Tests::assert(tree.get(1) == nullptr, "removeAndGet1 (not 1)", false)) return;
    if (!Tests::assert(tree.get(2) != nullptr, "removeAndGet1 (2)", false)) return;
    if (!Tests::assert(tree.get(3) != nullptr, "removeAndGet1 (3)", false)) return;

    Tests::assert(true, "removeAndGet1");
}
void Tests::removeAndGet2()
{
    BinaryTree tree;
    Tests::addElements(tree, 10, 5);
    for (int i = 2; i <= 10; i+= 2)
    {
        tree.remove(i);
    }

    for (int i = 2; i <= 10; i++)
    {
        if (i % 2 == 0)
        {
            if (!Tests::assert(tree.get(i) == nullptr, "removeAndGet2 (not " + to_string(i) + ")", false)) return;
        }
        else
        {
            if (!Tests::assert(tree.get(i) != nullptr, "removeAndGet2 (" + to_string(i) + ")", false)) return;
        }
    }

    Tests::assert(true, "removeAndGet2");
}
void Tests::removeAndGet3()
{
    BinaryTree tree;
    Tests::addElements(tree, 1000, 8);

    for (int i = 1; i <= 1000; i+= 5)
    {
        tree.remove(i);
    }

    for (int i = 1; i <= 1000; i++)
    {
        if ((i - 1) % 5 == 0)
        {
            if (!Tests::assert(tree.get(i) == nullptr, "removeAndGet3 (not " + to_string(i) + ")", false)) return;
        }
        else
        {
            if (!Tests::assert(tree.get(i) != nullptr, "removeAndGet3 (" + to_string(i) + ")", false)) return;
        }
    }

    Tests::assert(true, "removeAndGet3");
}
void Tests::compare1()
{
    BinaryTree tree1, tree2;

    Tests::addElements(tree1, 1000, 6);
    Tests::addElements(tree2, 1000, 6);

    Tests::assert(tree1.compareWith(tree2), "compare1");
}
void Tests::compare2()
{
    BinaryTree tree1, tree2;

    Tests::addElements(tree1, 1000, 6);
    Tests::addElements(tree2, 1000, 6);

    tree2.remove(7);

    Tests::assert(!tree1.compareWith(tree2), "compare1");
}
void Tests::clear()
{
    BinaryTree tree;

    Tests::addElements(tree, 200, 5);
    tree.clear();

    Tests::assert(tree.getSize() == 0, "clear");
}
void Tests::listNodes()
{
    BinaryTree tree;

    tree.add(5);
    tree.add(1);
    tree.add(7);
    tree.add(6);

    vector<const BinaryTree::Node*> nodes;
    tree.listNodes(nodes);

    if (!Tests::assert(nodes.size() == 4,"listNodes (count)", false)) return;
    if (!Tests::assert(nodes[0]->value == 5,"listNodes (node 1)", false)) return;
    if (!Tests::assert(nodes[1]->value == 1,"listNodes (node 2)", false)) return;
    if (!Tests::assert(nodes[2]->value == 7,"listNodes (node 3)", false)) return;
    if (!Tests::assert(nodes[3]->value == 6,"listNodes (node 4)", false)) return;

    Tests::assert(true, "listNodes");
}
void Tests::addTrees()
{
    BinaryTree tree1, tree2, result;
    Tests::addElements(tree1, 500, 3);
    Tests::addElements(tree2, 300, 7);

    BinaryTree::addTrees(tree1, tree2, result);

    Tests::assert(result.getSize() == 800, "addTrees");
}
void Tests::subtractTrees()
{
    BinaryTree tree1, tree2, result;
    Tests::addElements(tree1, 500, 2);
    Tests::addElements(tree2, 250, 2);

    BinaryTree::subtractTrees(tree1, tree2, result);

    Tests::assert(result.getSize() == 250, "subtractTrees");
}
void Tests::copy()
{
    BinaryTree tree1;
    Tests::addElements(tree1, 1000, 4);

    BinaryTree tree2(tree1);

    Tests::assert(tree1.compareWith(tree2), "copy");
}
void Tests::assignmentOperator()
{
    BinaryTree tree1;
    Tests::addElements(tree1, 1000, 4);

    BinaryTree tree2;
    Tests::addElements(tree2, 50, 2);
    tree2 = tree1;

    Tests::assert(tree1.compareWith(tree2), "assignmentOperator");
}
void Tests::equalsOperator()
{
    BinaryTree tree1, tree2;

    Tests::addElements(tree1, 1000, 6);
    Tests::addElements(tree2, 1000, 6);

    Tests::assert(tree1 == tree2, "equalsOperator");
}
void Tests::nonEqualsOperator()
{
    BinaryTree tree1, tree2;

    Tests::addElements(tree1, 1000, 6);
    Tests::addElements(tree2, 1000, 7);

    Tests::assert(tree1 != tree2, "nonEqualsOperator");
}
void Tests::addOperatorTrees1()
{
    BinaryTree tree1, tree2;
    Tests::addElements(tree1, 200, 3);
    Tests::addElements(tree2, 600, 7);

    BinaryTree result = tree1 + tree2;

    Tests::assert(result.getSize() == 800, "addOperatorTrees1");
}
void Tests::addOperatorTrees2()
{
    BinaryTree tree1, tree2;
    Tests::addElements(tree1, 200, 3);
    Tests::addElements(tree2, 600, 7);

    tree1 += tree2;

    Tests::assert(tree1.getSize() == 800, "addOperatorTrees2");
}
void Tests::subtractOperatorTrees1()
{
    BinaryTree tree1, tree2;
    Tests::addElements(tree1, 500, 2);
    Tests::addElements(tree2, 200, 2);

    BinaryTree result = tree1 - tree2;

    Tests::assert(result.getSize() == 300, "subtractOperatorTrees1");
}
void Tests::subtractOperatorTrees2()
{
    BinaryTree tree1, tree2;
    Tests::addElements(tree1, 500, 2);
    Tests::addElements(tree2, 200, 2);

    tree1 -= tree2;

    Tests::assert(tree1.getSize() == 300, "subtractOperatorTrees2");
}
void Tests::addOperatorValue1()
{
    BinaryTree tree;
    tree += 6;
    tree += 9;
    tree += 1;

    Tests::assert(tree.getSize() == 3, "addOperatorValue1");
}
void Tests::addOperatorValue2()
{
    BinaryTree tree;
    Tests::addElements(tree, 10, 1);
    BinaryTree tree2 = tree + 16;

    Tests::assert(tree.getSize() == 10 && tree2.getSize() == 11, "addOperatorValue2");
}
void Tests::removeOperatorValue1()
{
    BinaryTree tree;
    Tests::addElements(tree, 10, 1);

    tree -= 6;
    tree -= 32; // tree not contains it
    tree -= 9;

    Tests::assert(tree.getSize() == 8, "removeOperatorValue1");
}
void Tests::removeOperatorValue2()
{
    BinaryTree tree;
    Tests::addElements(tree, 10, 1);

    BinaryTree tree2 = tree - 6;
    BinaryTree tree3 = tree2 - 32; // tree not contains it
    BinaryTree tree4 = tree3 - 9;

    Tests::assert(tree.getSize() == 10 && tree2.getSize() == 9 && tree3.getSize() == 9 && tree4.getSize() == 8, "removeOperatorValue2");
}
void Tests::removeOperatorNode()
{
    BinaryTree tree;
    Tests::addElements(tree, 10, 1);

    const BinaryTree::Node* node = tree.get(3);
    tree -= node;

    Tests::assert(tree.getSize() == 9, "removeOperatorNode");
}
void Tests::accessorOperator()
{
    BinaryTree tree;
    Tests::addElements(tree, 10, 1);

    if (!Tests::assert(tree[5] != nullptr, "accessorOperator (exists)", false)) return;
    if (!Tests::assert(tree[30] == nullptr, "accessorOperator (exists)", false)) return;

    Tests::assert(true, "accessorOperator");
}
